package com.hyp.sinks

import com.hyp.utils.RedisUtils
import org.apache.spark.sql.{ForeachWriter, Row}

class RedisSink extends ForeachWriter[Row]{
  //初始化用的，可初始化数据库连接池
  override def open(partitionId: Long, epochId: Long): Boolean = true

//  override def process(value: Row): Unit = {
//    RedisUtils.lpush("rate",value.get(0)+":"+value.get(1)+":"+value.get(2))
//  }

  //遍历过程中做的操作
  override def process(value: Row): Unit = {
    RedisUtils.lpush("rate",value.get(0)+""+value.get(1))
  }

  //可以关闭连接池
  override def close(errorOrNull: Throwable): Unit = {}
}
