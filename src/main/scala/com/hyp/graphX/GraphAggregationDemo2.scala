package com.hyp.graphX

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, EdgeContext, Graph, VertexId}

object GraphAggregationDemo2 {
  def sendMsg(ec:EdgeContext[(Int,Int),String,(Int,Int)]):Unit = {
    //源点把自己当前的值+1传给目标定点，实现权值+1,如果有源点的值大于目标定点，送一个flag+1，只要flag不是0，就是需要再判断
    //防止为空的操作,如果没有源给自己发送值会为null
    ec.sendToSrc((0,0))
    if(ec.dstAttr._1 < ec.srcAttr._1 +1)
      ec.sendToDst((ec.srcAttr._1 +1,1))
    else
      ec.sendToDst((ec.srcAttr._1+1,0))
  }

  def mergeMsg(a: (Int,Int) , b:(Int,Int)) :(Int,Int) = {
    //有很多源点发给目标定点，其最大的距离才决定其权值，且max控制了+1之后
    (math.max(a._1,b._1), a._2+b._2)
  }

  def sumEdgeCount( g:Graph[(Int,Int),String]):Graph[(Int, Int), String] = {
    val verts = g.aggregateMessages[(Int,Int)]( sendMsg , mergeMsg)
    val g2: Graph[(Int, Int), String] = Graph(verts ,g.edges)
    val check = g2.vertices.filter(x=>x._2._2 >0).count()
//    println(check)
    if(check > 0) {
      val resetGraph = g2.mapVertices((vertexId:VertexId, attr:(Int,Int)) => {
        (attr._1,0)
      })
      sumEdgeCount(resetGraph)
    }
    else
      g
  }



  def main(args: Array[String]): Unit = {

    //设置运行环境
    val conf = new SparkConf().setAppName("SimpleGraphX").setMaster("local")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    // 构建图
    val myVertices = sc.parallelize(Array((1L, "张三"), (2L, "李四"), (3L, "王五"), (4L, "钱六"),
      (5L, "领导")))
    val myEdges = sc.makeRDD(Array( Edge(1L,2L,"朋友"),
      Edge(2L,3L,"朋友") , Edge(3L,4L,"朋友"),
      Edge(4L,5L,"上下级"),Edge(3L,5L,"上下级")
    ))

    val myGraph = Graph(myVertices,myEdges)

    val initGraph = myGraph.mapVertices((_,_)=>(0,0))

    val resultGraph = sumEdgeCount(initGraph)

    resultGraph.mapVertices(
      (vertexId,attr)=>{
        attr._1
      }
    ).vertices.collect().foreach(println(_))

  }
}
