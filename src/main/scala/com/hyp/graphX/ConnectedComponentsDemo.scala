package com.hyp.graphX

import org.apache.spark.graphx.{Edge, Graph, GraphLoader, VertexId, VertexRDD}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object ConnectedComponentsDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphX")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    val graph = GraphLoader.edgeListFile(sc,"C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\graph_followers.txt")

    /**
      *C onnected Components即连通体算法用id标注图中每个连通体，将连通体中序号最小的顶点的id作为连通体的id。
      * 任意2个顶点之间都存在路径，那么称为连通图，否则称该图为非连通图，其中的极大连通子图称为连通体
      * key为所有顶点的id,value为所在的连通体id
      * (1,1)
      * (4,1)
      * (3,3)
      * (6,3)
      * (7,3)
      * (2,1)
      * 注意:连通体的id为该连通图中最小的id
      */
    val components: VertexRDD[VertexId] = graph.connectedComponents().vertices
    println("ranks")
    components.foreach(println)
    val users: RDD[(Long, String)] = sc.textFile("C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\graph_users.txt").map(line=>{
      val fields = line.split(",")
      (fields(0).toLong,fields(1))
    })
    //join就是两rdd匹配
    val ranksByUsername: RDD[(String, Double)] = users.join(components).map{
      //rdd是返回左右匹配数据.id进行匹配，后面的元组是attr的集合
      case (id,(username,rank)) => (username,rank)
    }

    ranksByUsername.foreach(println)
  }
}
