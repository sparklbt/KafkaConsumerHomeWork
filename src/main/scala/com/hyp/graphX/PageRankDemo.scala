package com.hyp.graphX

import org.apache.spark.graphx.{GraphLoader, VertexRDD}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object PageRankDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphX")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

//    var lines = ("C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\graph_users")
      val graph = GraphLoader.edgeListFile(sc,"C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\graph_followers.txt")
      val ranks: VertexRDD[Double] = graph.pageRank(0.0001).vertices
      println("ranks")
      ranks.foreach(println)
      val users: RDD[(Long, String)] = sc.textFile("C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\graph_users.txt").map(line=>{
        val fields = line.split(",")
        (fields(0).toLong,fields(1))
      })
      //join就是两rdd匹配
      val ranksByUsername: RDD[(String, Double)] = users.join(ranks).map{
        //rdd是返回左右匹配数据.id进行匹配，后面的元组是attr的集合
        case (id,(username,rank)) => (username,rank)
      }
      println("ranksByUsername")
      println(ranksByUsername.collect().mkString("\n"))

  }
}
