package com.hyp.graphX

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, Graph, VertexId}
import org.apache.spark.rdd.RDD

object GraphMap {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphXMap")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")
    val users: RDD[(Long, (String, Int))] = sc.parallelize(Array((1L, ("Alice", 28)), (2L, ("richard", 27)), (3L, ("van", 65)), (4L, ("bili king", 42)), (5L, ("kkmax", 55)),(6L,("muji",50))))
    val edgeArray: RDD[Edge[Int]] = sc.parallelize(Array(Edge(2L,1L,7),Edge(2L,4L,2),Edge(3L,2L,4),Edge(3L,6L,3),Edge(4L,1L,1),Edge(5L,2L,2),Edge(5L,3L,8),Edge(5L,6L,3)))

    val graph = Graph(users,edgeArray)

    /**
      *     give edge's attr a new property
      */
    println("mapEdges")
    graph.mapEdges(edge=>{
      //method1 Edge(edge.srcId,edge.dstId,(edge.attr,"new value"))
      (edge.attr,"new value")
    }).edges.foreach(println)

    /**
      *   give triplet's last element:() a value (edge.attr, "add value"),
      *   that's what triplet's values below,two vertices and a new value,attr is edge's attr
        *   ((2,(richard,27)),(4,(bili king,42)),(2,add value))
        *   ((5,(kkmax,55)),(3,(van,65)),(8,add value))
        *   ((5,(kkmax,55)),(6,(muji,50)),(3,add value))
        *   ((5,(kkmax,55)),(2,(richard,27)),(2,add value))
        *   ((3,(van,65)),(6,(muji,50)),(3,add value))
        *   ((2,(richard,27)),(1,(Alice,28)),(7,add value))
        *   ((4,(bili king,42)),(1,(Alice,28)),(1,add value))
        *   ((3,(van,65)),(2,(richard,27)),(4,add value))
      */
    println("mapTriplet")
    graph.mapTriplets(triplet=>{
      (triplet.attr,"add value")
    }).triplets.foreach(println)

  }
}
