package com.hyp.graphX

import org.apache.spark.graphx.GraphLoader
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object TriangleDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphX")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    val graph = GraphLoader.edgeListFile(sc,"C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\graph_followers.txt")
    //统计每个顶点所在的三角形个数，如果两个点有共同邻居，三点就构成了三角形结果
    val triangleGraph = graph.triangleCount().vertices
    triangleGraph.foreach(println)
    val users: RDD[(Long, String)] = sc.textFile("C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\graph_users.txt").map(line=>{
      val fields = line.split(",")
      (fields(0).toLong,fields(1))
    })

    users.join(triangleGraph).map{
      case (id,(username,count)) => (username,count)
    }.foreach(println)
  }
}
