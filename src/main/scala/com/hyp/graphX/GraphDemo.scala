package com.hyp.graphX

import org.apache.spark.graphx.{Edge, EdgeTriplet, Graph, PartitionID, VertexId}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext, graphx}

object GraphDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphX")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")
    val users: RDD[(Long, (String, Int))] = sc.parallelize(Array((1L, ("Alice", 28)), (2L, ("richard", 27)), (3L, ("van", 65)), (4L, ("bili king", 42)), (5L, ("kkmax", 55)),(6L,("muji",50))))
    val edgeArray: RDD[Edge[Int]] = sc.parallelize(Array(Edge(2L,1L,7),Edge(2L,4L,2),Edge(3L,2L,4),Edge(3L,6L,3),Edge(4L,1L,1),Edge(5L,2L,2),Edge(5L,3L,8),Edge(5L,6L,3)))

    val graph = Graph(users,edgeArray)

    /**
      * where vertices.age >= 30
      */
    graph.vertices.filter{case (id,(name,age))=> age>=30}.map(x=>{
      (x._1.toLong,(x._2._1,x._2._2))
    }).foreach(println(_))

    /**
      * where edge's attr >=5
      */
    graph.edges.filter(x=> x.attr >=5).collect().foreach(println)

    println("max degree:"+graph.degrees.reduce((x,y)=>{if(x._2>y._2) x else y}))
    println("max inDegree:"+graph.inDegrees.reduce((x,y)=>{if(x._2>y._2) x else y}))
    println("max outDegree:"+graph.outDegrees.reduce((x,y)=>{if(x._2>y._2) x else y}))

//    /**
//      * 以下三个map为遍历改值操作(attr属性值),或者返回特定的结果也可以
//      */
//
//      //顶点的age值/2
//    val vertGraph: Graph[(VertexId, (String, PartitionID)), PartitionID] = graph.mapVertices((vertexId:VertexId, attr:(String,Int))=>{
//      (vertexId,(attr._1,attr._2/2))
//    })
//
//    println("vertGraph:")
//    vertGraph.vertices.collect().foreach(println)
//
//    /**
//      * src源顶点，dst目标顶点,所有边的attr也就是所带的值变为2倍
//      */
//    val edgeGraph: Graph[(String, PartitionID), Edge[PartitionID]] = graph.mapEdges((edge:Edge[Int])=>{
//      edge.attr = edge.attr*2
//      edge
//    })
//
//    println("edgeGraph:")
//    edgeGraph.edges.foreach(println)
//
//    //最后一个()里输出了dst的age*2,增加了一个属性值
//    val tripletGraph = graph.mapTriplets((triplet:EdgeTriplet[(String,Int),Int])=>{
//      triplet.dstAttr._2*2
//    })
//
//    //简单的遍历修改
//    val tripletGraph2 = graph.mapTriplets(triplet=>{
//      triplet.attr = triplet.attr*2
//    })
//    /**
//      * tripletGraph格式如下:
//      * ((5,(kkmax,55)),(6,(muji,50)),())
//      * ((2,(richard,27)),(1,(Alice,28)),())
//      * ((5,(kkmax,55)),(3,(van,65)),())
//      * ((2,(richard,27)),(4,(bili king,42)),())
//      * ((4,(bili king,42)),(1,(Alice,28)),())
//      * ((3,(van,65)),(6,(muji,50)),())
//      * ((5,(kkmax,55)),(2,(richard,27)),())
//      * ((3,(van,65)),(2,(richard,27)),())
//      */
//    println("tripletGraph:")
//    tripletGraph.triplets.foreach(println)
//    println("tripletGraph2:")
//    tripletGraph2.triplets.foreach(println)
  }
}
