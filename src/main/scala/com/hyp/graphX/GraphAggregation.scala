package com.hyp.graphX

import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object GraphAggregation {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphX")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    val users: RDD[(Long, Double)] = sc.parallelize(Array((1L, 28.toDouble), (2L, 27.toDouble), (3L,65.toDouble), (4L,42.toDouble), (5L, 55.toDouble),(6L,50.toDouble)))
    val edgeArray: RDD[Edge[Int]] = sc.parallelize(Array(Edge(2L,1L,7),Edge(2L,4L,2),Edge(3L,2L,4),Edge(3L,6L,3),Edge(4L,1L,1),Edge(5L,2L,2),Edge(5L,3L,8),Edge(5L,6L,3)))

    val graph = Graph(users,edgeArray)
    val aggregation = graph.aggregateMessages[(Double,Int)](
      triplet => {
        //如果源的年龄小于目标，发送(年龄+count)
        triplet.sendToDst(if(triplet.srcAttr < triplet.dstAttr)(triplet.srcAttr,1) else (0,0))
      },
      //reduce操作
      (a,b) => (a._1+b._1,a._2+b._2)
    ).mapValues((value)=>{
      //聚合操作
        if(value._2 != 0) value._1/value._2
        else 0
    })

    aggregation.collect().foreach(println)
  }
}
