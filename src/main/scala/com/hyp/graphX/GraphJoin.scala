package com.hyp.graphX

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.rdd.RDD

object GraphJoin {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphX")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")
    val users: RDD[(Long, (String, Int))] = sc.parallelize(Array((1L, ("Alice", 28)), (2L, ("richard", 27)), (3L, ("van", 65)), (4L, ("bili king", 42)), (5L, ("kkmax", 21)),(6L,("muji",50))))
    val edgeArray: RDD[Edge[Int]] = sc.parallelize(Array(Edge(2L,1L,7),Edge(2L,4L,2),Edge(3L,2L,4),Edge(3L,6L,3),Edge(4L,1L,1),Edge(5L,2L,2),Edge(5L,3L,8),Edge(5L,6L,3)))
    val graph = Graph(users,edgeArray)

    val relationship: RDD[(Long, (String, Boolean))] = sc.parallelize(Array((1L, ("Alice", true)), (2L, ("richard", false)), (3L, ("van", true)), (4L, ("bili king", false)),(6L,("muji",false))))
    /**
      * inner join ,first param is the RDD you want to make a relationship
      * second param is a map you want to deal with the relationship
      * value is relationship's attr,attr is graph vertices' attr
      */
      val outerJoinRDD = graph.outerJoinVertices(relationship)((id,attr,value)=>{
        value match {
          case Some(x) => if(x._2 ) (attr._1,attr._2+1) else (attr._1,attr._2-1)
          case None => (attr._1,0)
        }
      })

      outerJoinRDD.vertices.foreach(println)
  }
}
