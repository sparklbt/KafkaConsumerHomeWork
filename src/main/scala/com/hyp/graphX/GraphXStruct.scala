package com.hyp.graphX

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.rdd.RDD

object GraphXStruct {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]").setAppName("GraphXMap")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")
    val users: RDD[(Long, (String, Int))] = sc.parallelize(Array((1L, ("Alice", 28)), (2L, ("richard", 27)), (3L, ("van", 65)), (4L, ("bili king", 42)), (5L, ("kkmax", 55)),(6L,("muji",50))))
    val edgeArray: RDD[Edge[Int]] = sc.parallelize(Array(Edge(2L,1L,7),Edge(2L,4L,2),Edge(3L,2L,4),Edge(3L,6L,3),Edge(4L,1L,1),Edge(5L,2L,2),Edge(5L,3L,8),Edge(5L,6L,3)))

    val graph = Graph(users,edgeArray)
    println("reverse")

    /**
      * reverse vertices
      */
    graph.reverse.edges.foreach(println)

    /**
      * remove the edge that not exist in graph and the vertices as well
      * dpred filter edges，vpred filter vertices
      */
    println("subgraph")
    val graph2 = graph.subgraph(epred = (ed)=>ed.dstId!= 100L,vpred = (id,attr)=> id != 2L)

    /**
      * consturct a children graph with condition
      * return current graph and other graph's common graph
      */
    println("mask")
    graph.mask(graph2).edges.foreach(println)

    //merge
    println("merge:")
    graph.groupEdges(merge = (ed1,ed2) => (ed1+ed2)).edges.foreach(println)
  }
}
