package com.hyp.sparksql

import java.util.Properties

import org.apache.spark.sql.SparkSession

/**
  * 分析函数
  */
object AnalysisSql {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local[2]")
      .appName("sparkSqlExercise")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
    import spark.implicits._

    val properties = new Properties()
    properties.put("driver","com.mysql.jdbc.Driver")
    properties.put("user","root")
    properties.put("password","root")

    val grade = spark.read.jdbc("jdbc:mysql://localhost:3306/sparktest","analysis_grade",properties)
    grade.createOrReplaceTempView("grade")

    //这里的后面的意思是根据course分组，分数降序排列，来对成绩进行降序排列
    spark.sql("select *,rank() over(partition by course order by score desc) as rank from grade").show()

          //计算sum，不过每个name会出现多次，后面的sum值都一样,不如group by好用的感觉
        spark.sql("select name,sum(score) over(partition by name) as sum from grade ").show()

          //同上，不过是计算平均值
          spark.sql("select name,avg(score) over(partition by name) as avg from grade order by avg desc").show()
    spark.sql("select name,count(score) over(partition by name) as count from grade").show()

        //lag为去除最低的数值,lag(score,1)后面的数值为去掉多少个
    spark.sql("select name,lag from (select name,lag(score,1) over(partition by name order by score desc) as lag from grade) where lag is not null").show()

        //去掉一个最高的数值,lead(score,1)后面的数值为去掉多少个
    spark.sql("select name,lead from (select name,lead(score,1) over(partition by name order by score desc) as lead from grade) where lead is not null").show()

        //    该效果实现了获取每个人分数的最高分和最低分
    spark.sql("select name,course,score from (select *,lead(score,1) over(partition by name order by score desc) as lead,lag(score,1) over(partition by name order by score desc) as lag from grade) where lead is null or lag is null").show()

    //仅仅只把查到的数据第一行和第二行的score进行相加
    spark.sql("select name,course,score,sum(score) over(partition by name order by score desc range between 1 preceding and 2 following) as sum_value from grade").show()
  }
}
