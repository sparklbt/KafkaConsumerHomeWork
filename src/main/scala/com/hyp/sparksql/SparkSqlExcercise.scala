package com.hyp.sparksql

import java.util.Properties

import com.hyp.sparkudf.AverageUdf
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object SparkSqlExcercise {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local[2]")
      .appName("sparkSqlExercise")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
    import spark.implicits._
    val data = spark.read.json("C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\employee.json")
    println("1.")
    //查询所有数据
    data.show()
    //去除重复字段
    data.distinct().show()
    //去除id字段
    data.drop("id").show()
//    //筛选age > 30
    data.filter("age > 30").show()
    //按age分组
    data.groupBy("age").count().show()
    //按name升序
    data.orderBy(data.col("name")).show()
    //取出前三列
    data.show(3)
    //查询username并取别名username
    data.withColumn("username",data.col("name")).select("username").show()
    //查询年龄平均值
    data.agg(Map("age" -> "avg")).show()
    //查询年龄最小值
    data.agg(Map("age"->"min")).show()
//
//    println("2.")
    //rdd转化为dataFrame
    val rdd = spark.sparkContext.textFile("C:\\Users\\10441\\Desktop\\big data\\spark\\练习数据\\employee.txt")
    val df = rdd.map(_.split(",")).map(x=>(x(0),x(1),x(2))).toDF("id","name","age")
    df.show()

    println("3.")
    val properties = new Properties()
    properties.put("driver","com.mysql.jdbc.Driver")
    properties.put("user","root")
    properties.put("password","root")
    val mysqlConn = spark.read.jdbc("jdbc:mysql://127.0.0.1:3306/sparktest","employee",properties)
    mysqlConn.createOrReplaceTempView("employee")
//    val result = spark.sql("insert into table employee values('Mary','F',26),('Tom','M',23)")
//    result.write.mode(SaveMode.Append).jdbc("jdbc:mysql://127.0.0.1:3306/sparktest","employee",properties)
//    spark.sql("select max(age),sum(age) from employee").show()

//    val sqlFrame: DataFrame = spark.sparkContext.parallelize(Array("Mary F 26","Tom M 23")).map(_.split(" ")).map(x=>(x(0),x(1),x(2).toInt)).toDF("name","gender","age")
//    sqlFrame.write.mode(SaveMode.Append).jdbc("jdbc:mysql://127.0.0.1:3306/sparktest","employee",properties)

    //注册udf函数，直接使用在sql语句里
//    spark.udf.register("strLen", (str: String) => str.length())
    spark.udf.register("average",new AverageUdf())
    spark.sql("select average(age) from employee").show()
  }
}
