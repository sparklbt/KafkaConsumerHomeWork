package com.hyp.structredstreaming

import org.apache.spark.sql.streaming.{StreamingQuery, Trigger}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object kafkaDemo {

  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession
      .builder.master("local[2]")
      .appName("StructuredKafakDemo")
      .getOrCreate()
    spark.sqlContext.sparkContext.setLogLevel("WARN")
    import spark.implicits._

    // Create DataFrame representing the stream of input lines from connection to localhost:9999
    val df: DataFrame = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "learning:9092")
      .option("subscribe", "spark-topic-demo")
      .load()
    val words  = df.selectExpr( "CAST(value AS STRING)")
      .as[String].flatMap(_.split(" "))
    // Split the lines into words
    //val words: Dataset[String] = lines.as[String].flatMap(_.split(" "))


    // Generate running word count
    // val wordCounts: DataFrame = words.groupBy("value").count()
    val keyCounts = words.groupBy("value").count()
    //append获取实时数据，发啥接受啥,且一段时间内的慢慢叠加
    val query: StreamingQuery = keyCounts.writeStream.trigger(Trigger.ProcessingTime(3000))
      .outputMode("append")
      .format("console")
      .start()

    val writer = keyCounts.writeStream.trigger(Trigger.ProcessingTime(3000))
      .outputMode("update")
        .format("kafka")
        .option("kafka.bootstrap.servers","learning:9092")
        .option("topic","spark-topic-demo")
        .option("checkpointLocation","d:/kafka/tmp")
    query.awaitTermination()
  }

}
