package com.hyp.structredstreaming

import com.hyp.sinks.RedisSink
import com.hyp.utils.RedisUtils
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp
import org.apache.spark.sql.streaming.StreamingQueryListener

object forEachSinkDemo {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .master("local[2]")
      .appName("forEachDemo")
      .getOrCreate()

    spark.sparkContext.setLogLevel("WARN")
    //监听执行过程中的信息,只能这样才能获取batchId,和监控实时信息
    var batchId: Long = 0
    spark.streams.addListener(new StreamingQueryListener {
      override def onQueryStarted(event: StreamingQueryListener.QueryStartedEvent): Unit = {

      }

      override def onQueryProgress(event: StreamingQueryListener.QueryProgressEvent): Unit = {
        val progress = event.progress
        batchId = progress.batchId
        val inputRowsPerSecond: Double = progress.inputRowsPerSecond
        val processRowsPerSecond: Double = progress.processedRowsPerSecond
        val numInputRows: Long = progress.numInputRows
        println("batchId=" + batchId, "  numInputRows=" + numInputRows + "  inputRowsPerSecond=" + inputRowsPerSecond +
          "  processRowsPerSecond=" + processRowsPerSecond)
      }
      override def onQueryTerminated(event: StreamingQueryListener.QueryTerminatedEvent): Unit = {}

    })

    //这个包才能解析as[]类型
    import spark.implicits._

    //kafka输入源
    val sources = spark.readStream
        .format("kafka")
        .option("kafka.bootstrap.servers","learning:9092")
        .option("subscribe","spark-topic-demo")
        .option("rowsPerSecond", 100)
      .load()

//    val lines: DataFrame = spark.readStream
//      .format("socket")
//      .option("host", "192.168.158.150")
//      .option("port", 6667)
//      .load()
//    val tuple = sources.selectExpr("CAST(timestamp AS TIMESTAMP)","CAST(value AS STRING)").as[(Timestamp,String)].map()

    //kafka会发送结构化数据过来，在这里select想要的字段并作操作(这里取value和timestamp),具体有哪些自己全部打印看一下
    val df: DataFrame = sources.selectExpr("CAST(timestamp AS TIMESTAMP)","CAST(value AS STRING)").as[(Timestamp,String)].flatMap(x=>{x._2.split(" ").map(y=>(x._1,y))}).map{
      case (timestamp: Timestamp,value:String) =>
        val tuple = (batchId,timestamp,value)
        tuple
    }.toDF("batchId","timestamp","value")
//    val df: Dataset[String] = lines.as[String].flatMap(_.split(" "))
    val words: DataFrame = df.select("value","timestamp")

    //append模式追加,complete模式获取全部内容,update好像跟append差不多,redisSink是自己写的存入redis的实现类
    words.writeStream
      .outputMode("append")
//      .option("checkpointLocation","d:/kafka/tmp")
      .foreach(new RedisSink())
    //也可以这样
    /*.foreach(new ForeachWriter[Row](){
      override def open(partitionId: Long, version: Long): Boolean ={。。。。。}
      override def process(value: Row): Unit ={。。。。。。}
      override def close(errorOrNull: Throwable): Unit ={。。。。。。}
    })*/
      .start()
      .awaitTermination()
  }
}
