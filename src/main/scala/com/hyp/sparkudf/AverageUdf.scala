package com.hyp.sparkudf

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types.{DataType, DoubleType, IntegerType, StructField, StructType}

//udf就是sql的函数，这是自己写的udaf
class AverageUdf extends UserDefinedAggregateFunction{

  //输入的数据库数据类型
  override def inputSchema: StructType = StructType(StructField("age",IntegerType) :: Nil)

  //在aggregation里的value type
  override def bufferSchema: StructType = StructType(StructField("sum",IntegerType) :: StructField("count",IntegerType) :: Nil)

  //返回结果的type
  override def dataType: DataType = DoubleType

  //输入值一样，输出是否一致
  override def deterministic: Boolean = true

  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    //对上方bufferSchema的初始值声明
    buffer(0) = 0
    buffer(1) = 0
  }

  //要做的update操作
  //对buffer中初始化的值进行update,与input进来的值进行运算,sum实现相加，count实现加1
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    buffer(0) = buffer.getInt(0) + input.getInt(0)
    buffer(1) = buffer.getInt(1)+1
  }

  //合并缓冲区的数据，将不同分区的数据合并成一个,sum和count数据分别聚合
  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    buffer1(0) = buffer1.getInt(0) + buffer2.getInt(0)
    buffer1(1) = buffer1.getInt(1) + buffer2.getInt(1)
  }

  //最终的整体输出结果,buffer1为总和，buffer2为count
  override def evaluate(buffer: Row): Any = {
    buffer.getInt(0).toDouble / buffer.getInt(1)
  }
}
